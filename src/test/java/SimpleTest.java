import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SimpleTest {

    @BeforeClass(description = "BEFORE class")
    @Description("BEFORE class")
    public void beforeTest() {
        oneStep();
        oneStep();
    }

    @Test(description = "Test ONE")
    @Description("Test ONE")
    public void test1() {
        oneStep();
        oneStep();
        oneStep();
    }

    @Test(description = "Test TWO")
    @Description("Test TWO")
    public void test2() {
        oneStep();
        oneStep();
        oneStep();
    }



    @Step("Step 1")
    private void oneStep() {

    }


}
